package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.*;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class BattleTest {
    Army armyOne = new Army("TestArmyOne");
    Army armyTwo = new Army("InfantryArmy");
    ArrayList<Unit> cavalryList = new ArrayList<>();
    ArrayList<Unit> infantryList = new ArrayList<>();
    ArrayList<Unit> rangedList = new ArrayList<>();

    public void makeArmies(){
        for(int i = 0; i < 50; i++){
            cavalryList.add(new CavalryUnit("CavalryUnit",100));
            infantryList.add(new InfantryUnit("InfantryUnit",100));
            rangedList.add(new RangedUnit("RangedUnit",100));
        }

        armyOne.addAll(cavalryList);
        armyOne.addAll(rangedList);

        armyTwo.addAll(infantryList);
        armyTwo.add(new CommanderUnit("Commander",180));
    }

    @Test
    void testArmyCreation(){
        makeArmies();

        assertEquals(rangedList,armyOne.getRangedUnits());
        assertEquals(cavalryList,armyOne.getCavalryUnits());
        assertEquals(infantryList,armyTwo.getInfantryUnits());
        assertEquals(1,armyTwo.getCommanderUnits().size());

        testBattle();
    }

    public void testBattle(){
        Battle testBattle = new Battle(armyOne,armyTwo);

        Army winningArmy = testBattle.simulate(Terrain.HILL);

        if(winningArmy.equals(armyOne) && !armyOne.hasUnits() || winningArmy.equals(armyTwo) && !armyTwo.hasUnits()){
            fail("An army with 0 units remaining won.");
        }
    }

}