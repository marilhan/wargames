package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.CavalryUnit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CavalryUnitTest{
    @Test
    void createCalvaryUnitTest() {

        CavalryUnit cavalryUnit = new CavalryUnit("Cavalry1",100);
        Terrain terrain = Terrain.PLAINS;
        assertEquals("Cavalry1",cavalryUnit.getName());
        assertEquals(100,cavalryUnit.getHealth());
        assertEquals(20,cavalryUnit.getAttack());
        assertEquals(12,cavalryUnit.getArmor());
        assertEquals(8,cavalryUnit.getAttackBonus(terrain));
        assertEquals(1,cavalryUnit.getResistBonus(terrain));
    }

}