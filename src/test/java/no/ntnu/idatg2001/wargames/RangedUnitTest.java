package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.RangedUnit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class RangedUnitTest {
    @Test
    void createRangedUnitTest() {
        RangedUnit rangedUnit = new RangedUnit("Ranged1",100);

        assertEquals("Ranged1",rangedUnit.getName());
        assertEquals(100,rangedUnit.getHealth());
        assertEquals(15,rangedUnit.getAttack());
        assertEquals(8,rangedUnit.getArmor());
        assertEquals(6,rangedUnit.getAttackBonus(Terrain.HILL));
        assertEquals(6,rangedUnit.getResistBonus(Terrain.HILL));
    }

}