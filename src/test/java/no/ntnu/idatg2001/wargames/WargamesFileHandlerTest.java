package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.CavalryUnit;
import no.ntnu.idatg2001.wargames.Units.RangedUnit;
import no.ntnu.idatg2001.wargames.Units.Unit;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class WargamesFileHandlerTest {
    @Test
    void saveArmyTest() throws IOException {
        ArrayList<Unit> testArmy = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            testArmy.add(new CavalryUnit("Knight",100));
            testArmy.add(new RangedUnit("Archer",100));
        }
        Army army = new Army("Temp army for tests",testArmy);
        WargamesFileHandler armyToFile = new WargamesFileHandler();
        armyToFile.saveArmy(army);
    }

    @Test
    void testException() throws IOException {
        Army army = new Army("Test Army For Exception");
        army.add(new CavalryUnit("Exception,unit",100));
        assertThrows(RuntimeException.class,
                ()->{
                    WargamesFileHandler armyToFile = new WargamesFileHandler();
                    armyToFile.saveArmy(army);
                });

        File file = new File("src/main/resources/Armies/Test-Army-For-Exception.csv");
        if (file.delete()) {
            System.out.println("File deleted successfully");
        }
        else {
            System.out.println("Failed to delete the file");
        }
    }

    @Test
    void readFromFileTest() throws IOException {
        File file = new File("src/main/resources/Armies/Temp-army-for-tests.csv");
        WargamesFileHandler fileHandler = new WargamesFileHandler();

        Army army;
        army = fileHandler.loadArmyFromFile(file);

        assertEquals(20,army.getUnits().size());

        file.delete();
    }

}