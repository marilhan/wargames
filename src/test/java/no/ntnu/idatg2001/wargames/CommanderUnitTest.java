package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.CommanderUnit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CommanderUnitTest {
    @Test
    void createCommanderUnitTest() {
        CommanderUnit commanderUnit = new CommanderUnit("Commander1",100);

        assertEquals("Commander1",commanderUnit.getName());
        assertEquals(100,commanderUnit.getHealth());
        assertEquals(25,commanderUnit.getAttack());
        assertEquals(15,commanderUnit.getArmor());
        assertEquals(8,commanderUnit.getAttackBonus(Terrain.PLAINS));
        assertEquals(1,commanderUnit.getResistBonus(Terrain.PLAINS));
    }

}