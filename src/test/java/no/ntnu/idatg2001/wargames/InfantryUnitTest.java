package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.InfantryUnit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest{

    @Test
    void createInfantryUnitTest() {
        InfantryUnit infantryUnit = new InfantryUnit("Infantry1",100);

        assertEquals("Infantry1",infantryUnit.getName());
        assertEquals(100,infantryUnit.getHealth());
        assertEquals(15,infantryUnit.getAttack());
        assertEquals(10,infantryUnit.getArmor());
        assertEquals(5,infantryUnit.getAttackBonus(Terrain.FOREST));
        assertEquals(4,infantryUnit.getResistBonus(Terrain.FOREST));
    }


    @Test
    void testException(){
        assertThrows(IllegalArgumentException.class,
                ()->{
                new InfantryUnit("Infantry1",-1);
                });
    }
}