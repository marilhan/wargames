package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.*;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class ArmyTest{

    Army armyOne = new Army("Army One");
    ArrayList<Unit> unitTestList = new ArrayList<>();

    @Test
    void MakeArmyTest(){

        unitTestList.add(new InfantryUnit("Infantry1",100));
        unitTestList.add(new RangedUnit("Ranged1",100));
        unitTestList.add(new CavalryUnit("Cavalry1",100));
        unitTestList.add(new CommanderUnit("Commander1",100));

        armyOne.addAll(unitTestList);

        assertEquals(true,armyOne.hasUnits());

        System.out.println(armyOne.getUnits());
        getRandomUnitTest();
    }

    //tests equals and random method.
    public void getRandomUnitTest() {

        Unit randomUnit = armyOne.getRandom();

        for (Unit u : unitTestList) {
            if (randomUnit.equals(u)){
                assertEquals(u,randomUnit);
                System.out.println(randomUnit);
            }
        }

        removeUnitTest();
    }

    public void removeUnitTest(){

        Unit removeUnit = armyOne.getRandom();

        armyOne.remove(removeUnit);

        for(Unit u : armyOne.getUnits()){
            if(removeUnit.equals(u)){
                fail("Unit did not get removed!");
            }
        }

    }


}