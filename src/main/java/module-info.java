module no.ntnu.idatg2001.wargames {
    requires javafx.controls;
    requires javafx.graphics;
    requires java.desktop;
    requires javafx.fxml;
    exports no.ntnu.idatg2001.wargames.Controllers;
    exports no.ntnu.idatg2001.wargames;
    opens no.ntnu.idatg2001.wargames;
    opens no.ntnu.idatg2001.wargames.Controllers to javafx.fxml;
}