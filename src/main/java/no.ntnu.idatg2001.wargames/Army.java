package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * The Army class represents a single army.
 */
public class Army {
    /**
     * name of the army as String
     */
    private String name;

    /**
     * A list that will hold the units in the army
     */
    private ArrayList<Unit> units;

    public Army(){
        this.name = new String();
        this.units = new ArrayList<>();
    }

    /**
     * Creates an Army with input name
     * @param name the name of the unit. Fails if input is empty
     */
    public Army(String name){
        this.name = name;
        this.units = new ArrayList<>();

        if(name.isBlank())
            throw new IllegalArgumentException("Army name cannot be empty");
        if(name.contains(","))
            throw new IllegalArgumentException("Name cannot have a comma");

    }

    /**
     * Creates an Army with input name and units
     * @param name the name of the army. Fails if input is empty
     * @param units list of units as ArrayList
     */
    public Army(String name, ArrayList<Unit> units){
        this.name = name;
        this.units = units;

        if(name.isBlank())
            throw new IllegalArgumentException("Army name cannot be empty or have comma");
    }

    /**
     * gets the name of the army
     * @return name as String
     */
    public String getName() {
        return name;
    }

    /**
     * sets the name of the army to input value
     * @param name name as String
     */
    public void setName(String name) {
        this.name = name;
        if(name.isBlank())
            throw new IllegalArgumentException("Army name cannot be empty");
    }

    /**
     * gets the list of units
     * @return units as ArrayList
     */
    public ArrayList<Unit> getUnits() {
        return units;
    }

    /**
     * method to add a Unit to the ArrayList of units
     * @param unit Unit to be added
     */
    public void add(Unit unit){
        this.units.add(unit);
    }

    /**
     * method to add all elements of input list to the units list
     * @param listOfUnits a list of units to be added to the main units list
     */
    public void addAll(ArrayList<Unit> listOfUnits){
        for(Unit u:listOfUnits){
            add(u);
        }
    }

    /**
     * a method to remove a specific unit from units list
     * @param unit Unit to be removed from units list
     */
    public void remove(Unit unit){
        this.units.remove(unit);
        units.trimToSize();
    }

    /**
     * a method to check if units list has units or is empty
     * @return boolean, true if not empty, false if empty
     */
    public boolean hasUnits(){
        return units.size() > 0;
    }

    /**
     * selects and returns a random unit from the units list
     * @return a random Unit from units list
     */
    public Unit getRandom(){
        Random generate = new Random();
        return units.get(generate.nextInt(units.size()));
    }

    /**
     * a method to get all InfantryUnits in the army's units list
     * @return an ArrayList of all infantry units
     */
    public ArrayList<Unit> getInfantryUnits(){
        return (ArrayList<Unit>) units.stream()
                .filter((Unit unit) -> {return unit instanceof InfantryUnit;})
                .collect(Collectors.toList());
    }

    /**
     * a method to get all CavalryUnits in the army's units list
     * @return an ArrayList of all cavalry units
     */
    public ArrayList<Unit> getCavalryUnits(){
        return (ArrayList<Unit>) units.stream()
                .filter((Unit unit) -> {return unit instanceof CavalryUnit;})
                .collect(Collectors.toList());
    }

    /**
     * a method to get all RangedUnits in the army's units list
     * @return an ArrayList of all ranged units
     */
    public ArrayList<Unit> getRangedUnits(){
        return (ArrayList<Unit>) units.stream()
                .filter((Unit unit) -> {return unit instanceof RangedUnit;})
                .collect(Collectors.toList());
    }

    /**
     * a method to get all CommanderUnits in the army's units list
     * @return an ArrayList of all commander units
     */
    public ArrayList<Unit> getCommanderUnits(){
        return (ArrayList<Unit>) units.stream()
                .filter((Unit unit) -> {return unit instanceof CommanderUnit;})
                .collect(Collectors.toList());
    }

    /**
     * Overrides the equals method
     * @param o the object for comparison
     * @return boolean, true if the objects equals each other
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name,army.name) && Objects.equals(units, army.units);
    }

    /**
     * Overrides the hashCode method to fit the class
     * @return hashcode of object
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }

    /**
     * Override the toString function to fit desired format
     * @return The army values as a String
     */
    @Override
    public String toString() {
        return"Army{" +
                "Name=" + name + "\n" +
                ", Units=" + units +
                "}";
    }

}
