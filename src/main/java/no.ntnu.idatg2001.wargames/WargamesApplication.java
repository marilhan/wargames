package no.ntnu.idatg2001.wargames;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The main application for the project
 */
public class WargamesApplication extends Application {
    /**
     * The WargamesApplication class is main class to be used for initiating the application
     * @param args
     */

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Initializes the GUI with the first fxml file
     * @param primaryStage stage for home page
     * @throws Exception throws exception if FXMLLoader struggles to load the resource
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(WargamesApplication.class.getResource("/HomePage.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("War Games");
        primaryStage.show();
    }

}
