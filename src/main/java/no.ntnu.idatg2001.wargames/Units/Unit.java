package no.ntnu.idatg2001.wargames.Units;

import no.ntnu.idatg2001.wargames.Terrain;

import java.util.Objects;

/**
 * The Unit class represents a single unit in an Army
 */
public abstract class Unit {

    /**
     * the name of the unit as string
     */
    private final String name;

    /**
     * the health of the unit as int
     */
    private int health;

    /**
     * the attack of the unit as int
     */
    private int attack;

    /**
     * the armor of the unit as int
     */
    private int armor;

    /**
     * Creates a unit with input name, health, attack and armor.
     * Fails if inputs are invalid
     * @param name the name of the unit. Fails if input is an empty string
     * @param health the health of the unit. Fails if input is less than 0
     * @param attack the attack of the unit. Fails if input is less than 0
     * @param armor the armor of the unit. Fails if the input is less than 0
     */
    protected Unit(String name, int health, int attack, int armor) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;

        if(name.isBlank())
            throw new IllegalArgumentException("Name cannot be empty");

        if(health < 0)
            throw new IllegalArgumentException("Health cannot be lower than 0");

        if(attack < 0)
            throw new IllegalArgumentException("Attack cannot be lower than 0");

        if(armor < 0)
            throw new IllegalArgumentException("Armor cannot be lower than 0");
    }

    /**
     * a method to get the name of the unit
     * @return the name as String
     */
    public String getName() {
        return name;
    }

    /**
     * a method to get the health of the unit
     * @return the health as int
     */
    public int getHealth() {
        return health;
    }

    /**
     * a method to get the attack of the unit
     * @return the attack as int
     */
    public int getAttack() {
        return attack;
    }

    /**
     * a method to get the armor of the unit
     * @return the armor as int
     */
    public int getArmor() {
        return armor;
    }

    /**
     * a method to set the health of the unit
     * @param health current health of unit
     */
    public void setHealth(int health) {
        this.health = Math.max(health,0);
    }

    /**
     * a method to get the attack bonus
     * @return attack bonus as int
     */
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * a method to get the resist bonus
     * @return the resist bonus as int
     */
    public abstract int getResistBonus(Terrain terrain);

    /**
     * a method to attack another unit
     * @param opponent the unit to attack
     */
    public void attack(Unit opponent,Terrain currentTerrain){
        int myAttack = this.getAttack() + this.getAttackBonus(currentTerrain);
        int opDefence = opponent.getArmor() + opponent.getResistBonus(currentTerrain);
        opponent.setHealth(opponent.getHealth() - myAttack + opDefence);
    }

    /**
     * Overrides the equals method
     * @param o the object for comparison
     * @return boolean, true if the objects equals each other
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unit unit = (Unit) o;
        return health == unit.health && attack == unit.attack && armor == unit.armor && Objects.equals(name, unit.name);
    }

    /**
     * Overrides the hashCode method
     * @return hashcode of object
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, health, attack, armor);
    }

    /**
     * Override the toString function to fit desired format
     * @return The unit values as String
     */
    @Override
    public String toString() {
        return "Type: " + getClass().getSimpleName() + "\n" +
                "Name: " + name + "\n" +
                "Health: " + health + "\n" +
                "Attack: " + attack + "\n" +
                "Armor: " + armor;
    }

}


