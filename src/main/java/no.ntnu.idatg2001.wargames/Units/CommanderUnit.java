package no.ntnu.idatg2001.wargames.Units;

import no.ntnu.idatg2001.wargames.Units.CavalryUnit;

/**
 * The InfantryUnit class extends CavalryUnit and represents a single commander unit in an army
 */
public class CommanderUnit extends CavalryUnit {

    /**
     * Instantiates a Commander unit with input name, health, attack and armor
     * @param name the name of the unit.
     * @param health the health of the unit.
     * @param attack the attack of the unit.
     * @param armor the armor of the unit.
     */
    public CommanderUnit(String name, int health, int attack, int armor){
        super(name,health,attack,armor);
    }

    /**
     * Instantiates a Commander unit with input name, health and predetermined attack/armor
     * @param name name of unit
     * @param health health of unit
     */
    public CommanderUnit(String name, int health) {
        super(name,health,25,15);
    }
}
