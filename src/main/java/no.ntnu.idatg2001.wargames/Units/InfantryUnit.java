package no.ntnu.idatg2001.wargames.Units;

import no.ntnu.idatg2001.wargames.Terrain;

/**
 * The InfantryUnit class extends unit and represents a single infantry unit in an army
 */
public class InfantryUnit extends Unit {

    /**
     * Instantiates an Infantry unit with input name, health, attack and armor
     * @param name the name of the unit.
     * @param health the health of the unit.
     * @param attack the attack of the unit.
     * @param armor the armor of the unit.
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }


    /**
     * Instantiates an Infantry unit with input name, health and predetermined attack/armor
     * @param name name of unit
     * @param health health of unit
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     * gets attack bonus specific for Infantry unit
     * @return 2 as int
     */
    @Override
    public int getAttackBonus(Terrain terrain){
        if(terrain == Terrain.FOREST){
            return 5;
        }
        else{
            return 2;
        }
    }

    /**
     * gets resist bonus specific for Infantry unit
     * @return 1 as int
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        if(terrain == Terrain.FOREST){
            return 4;
        }
        else{
            return 1;
        }
    }
}
