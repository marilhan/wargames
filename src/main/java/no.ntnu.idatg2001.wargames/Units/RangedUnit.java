package no.ntnu.idatg2001.wargames.Units;

import no.ntnu.idatg2001.wargames.Terrain;
import no.ntnu.idatg2001.wargames.Units.Unit;

/**
 * The RangedUnit class extends unit and represents a single ranged unit in an army
 */
public class RangedUnit extends Unit {

    /**
     * Instantiates a Ranged unit with input name, health, attack and armor
     * @param name the name of the unit.
     * @param health the health of the unit.
     * @param attack the attack of the unit.
     * @param armor the armor of the unit.
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a Ranged unit with input name, health and predetermined attack/armor
     * @param name name of unit
     * @param health health of unit
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    /**
     * gets attack bonus specific to Ranged unit
     * @return 3 as int
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = 2;
        if(terrain == Terrain.HILL){
            attackBonus += 4;
        }
        if(terrain == Terrain.FOREST){
            attackBonus += 1;
        }
        return attackBonus;
    }

    /**
     * Temp solution to ranged unit having less resist if attacked multiple times
     */
    private int timesAttacked = 0;
    /**
     *
     * @return int depending on how many times it has been attacked
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        timesAttacked++;
        if(timesAttacked == 1){
            return 6;
        }
        if(timesAttacked == 2){
            return 4;
        }
        else{
            return 2;
        }
    }
}
