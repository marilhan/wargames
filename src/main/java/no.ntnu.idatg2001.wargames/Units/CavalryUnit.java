package no.ntnu.idatg2001.wargames.Units;

import no.ntnu.idatg2001.wargames.Terrain;
import no.ntnu.idatg2001.wargames.Units.Unit;

/**
 * The CavalryUnit class extends unit and represents a single cavalry unit in an army
 */
public class CavalryUnit extends Unit {
    /**
     * Instantiates a Cavalry unit with input name, health, attack and armor
     * @param name the name of the unit.
     * @param health the health of the unit.
     * @param attack the attack of the unit.
     * @param armor the armor of the unit.
     */
    public CavalryUnit(String name, int health, int attack, int armor){
        super(name, health, attack, armor);
        this.hasAttacked = false;
    }

    /**
     * Instantiates a Cavalry unit with input name, health and predetermined attack/armor
     * @param name name of unit
     * @param health health of unit
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
        this.hasAttacked = false;
    }

    /**
     * gets attack bonus specific for Cavalry unit
     * If cavalry unit has already attacked it deals less damage
     * @return int based on times it has attacked
     */
    private boolean hasAttacked;
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = 2;
        if(!hasAttacked){
            attackBonus += 4;
            hasAttacked = true;
        }
        if(terrain == Terrain.PLAINS){
            attackBonus += 2;
        }
        return attackBonus;
    }

    /**
     * gets resist bonus specific to Cavalry unit
     * @return 1 as int
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        if(terrain == Terrain.FOREST){
            return 0;
        }
        else{
            return 1;
        }
    }

}
