package no.ntnu.idatg2001.wargames;

/**
 * ENUM to be used when simulating a battle
 */
public enum Terrain {
    HILL,
    PLAINS,
    FOREST
}
