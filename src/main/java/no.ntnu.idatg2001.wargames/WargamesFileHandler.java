package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.*;

import java.io.*;
import java.io.File;

import static java.lang.Integer.parseInt;

/**
 * The WargamesFileHandler class controls the usage of external documents
 * by taking an army and saving it to a new file
 */
public class WargamesFileHandler {
    private static Army army;
    public static final String delimiter = ",";

    /**
     * constructor to initiate local variable army to be used in methods in the class
     */
    public WargamesFileHandler(){}

    /**
     * a method for creating a .csv file and saving an army to that file
     * @throws IOException if the file it is trying to create already exsits
     */

    public static void saveArmy(Army armyToSave) throws IOException {
        army = armyToSave;
        String armyName = army.getName();
        if(armyName.contains(" ")){
            armyName = armyName.replaceAll(" ","-");
        }
        File csvFile = new File("src/main/resources/Armies/" + armyName + ".csv");
        if(csvFile.isFile() && !csvFile.isDirectory()) {
            throw new IOException("An army with that name already exists");
        }
        else{
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(csvFile))) {
                bw.write(getArmyInfo());
            } catch(Exception e){
                throw new RuntimeException("Could not write to file");
            }
        }
    }

    /**
     * a method for converting the units and it's fields in an army to a CSV friendly
     * string
     * @return a string containing the units in an army and the army name
     */

    private static String getArmyInfo() throws IllegalArgumentException{
        String armyInfo = WargamesFileHandler.army.getName() + "\n";
        for(Unit unit : WargamesFileHandler.army.getUnits()) {
            if (!unit.getName().contains(delimiter)) {
                armyInfo += unit.getClass().getSimpleName() + delimiter + unit.getName() + delimiter + unit.getHealth() + "\n";
            }
            else {
                throw new IllegalArgumentException("Unit "+unit.getName() +" has a comma in the name");
            }
        }
        return armyInfo;
    }

    /**
     * Method to load an army from a CSV file
     * @param file the selected file to load the army from
     * @return the army read from the file as Army
     * @throws IOException if the reader cant find the file
     */
    public static Army loadArmyFromFile(File file) throws IOException {
        Army newArmy = new Army();
        try(BufferedReader reader = new BufferedReader(new FileReader(file))){
            newArmy.setName(reader.readLine());
            String unitLine = "";
            String[] unitAttributes;
            while ((unitLine = reader.readLine()) != null){
                unitAttributes = unitLine.split(delimiter);
                switch (unitAttributes[0]){
                    case "InfantryUnit" -> newArmy.add(new InfantryUnit(unitAttributes[1],parseInt(unitAttributes[2])));
                    case "CavalryUnit" -> newArmy.add(new CavalryUnit(unitAttributes[1],parseInt(unitAttributes[2])));
                    case "RangedUnit" -> newArmy.add(new RangedUnit(unitAttributes[1],parseInt(unitAttributes[2])));
                    case "CommanderUnit" -> newArmy.add(new CommanderUnit(unitAttributes[1],parseInt(unitAttributes[2])));
                    default -> throw new RuntimeException("There was a problem reading from file");
                };
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return newArmy;
    }

    /**
     * method to delete a file conating an army
     * @param file file to be deleted
     * @throws FileNotFoundException if the file is not found
     */
    public void deleteArmy(File file) throws FileNotFoundException {
        if(file.exists()) {
            file.delete();
        }
        else {
            throw new FileNotFoundException("File with path " + file.getPath() + " does not exist");
        }
    }
}
