package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.*;

import java.util.ArrayList;

/**
 * The unit factory makes a list of units based on input parameters
 */
public class UnitFactory {
    /**
     * Method takes input of a unit type, name and health, and creates a new unit of said type.
     * @param unitType the type of unit to be created
     * @param unitName the name of the unit
     * @param unitHealth the health of the unit
     * @return returns the Unit that was made
     * @throws IllegalArgumentException throws exception if the input unit type does not exist
     */
    public Unit createUnit(String unitType,String unitName, int unitHealth) throws IllegalArgumentException{
        return switch (unitType){
            case "InfantryUnit" -> new InfantryUnit(unitName,unitHealth);
            case "CavalryUnit" -> new CavalryUnit(unitName,unitHealth);
            case "RangedUnit" -> new RangedUnit(unitName,unitHealth);
            case "CommanderUnit" -> new CommanderUnit(unitName,unitHealth);
            default -> throw new IllegalArgumentException("Could not make unit of type " + unitType);
        };
    }

    /**
     * Method to generate a list of units based on input parameters
     * @param unitType the type of unit to be made
     * @param amountOfUnits the amount of units to be made
     * @param unitName the name of the units
     * @param unitHealth the health of the units
     * @return an ArrayList of units
     */
    public ArrayList<Unit> makeListOfUnits(String unitType, int amountOfUnits, String unitName, int unitHealth){
        ArrayList<Unit> unitList = new ArrayList<>();
        int i = 0;
        while(i < amountOfUnits) {
            unitList.add(createUnit(unitType,unitName,unitHealth));
            i++;
        }
        return unitList;
    }
}
