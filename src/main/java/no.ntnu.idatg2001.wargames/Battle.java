package no.ntnu.idatg2001.wargames;

import no.ntnu.idatg2001.wargames.Units.Unit;

/**
 * The Battle class represents a battle between two armies
 */
public class Battle {

    /**
     * Defines the two armies
     */
    private Army armyOne;
    private Army armyTwo;

    public Battle(Army armyOne, Army armyTwo){
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;

        if(!armyOne.hasUnits())
            throw new IllegalArgumentException("ArmyOne has no units");

        if(!armyTwo.hasUnits())
            throw new IllegalArgumentException("ArmyTwo has no units");
    }

    /**
     * a method to simulate a battle between two armies
     * @return the winner of the battle as Army
     */
    public Army simulate(Terrain terrain){
        while(armyOne.hasUnits() && armyTwo.hasUnits()){
            Unit attacker = armyOne.getRandom();
            Unit opponent = armyTwo.getRandom();
            attacker.attack(opponent,terrain);
            if(opponent.getHealth()<=0){
                armyTwo.remove(opponent);
            }
            if(armyOne.hasUnits() && armyTwo.hasUnits()) {
                Unit attacker2 = armyTwo.getRandom();
                Unit opponent2 = armyOne.getRandom();
                attacker2.attack(opponent2,terrain);
                if (opponent2.getHealth() <= 0) {
                    armyOne.remove(opponent2);
                }
            }
        }
        if(armyOne.hasUnits()){
            return armyOne;
        }
        else{
            return armyTwo;
        }
    }

    /**
     * Override the toString function to fit desired format
     * @return the battle values as a String
     */
    @Override
    public String toString() {
        return"Battle{" +
                "armyOne=" + armyOne  +
                ", armyTwo=" + armyTwo +
                "}";
    }
}
