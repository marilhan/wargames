package no.ntnu.idatg2001.wargames.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.idatg2001.wargames.Army;
import no.ntnu.idatg2001.wargames.Battle;
import no.ntnu.idatg2001.wargames.Terrain;
import no.ntnu.idatg2001.wargames.WargamesFileHandler;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the main page for the wargames application
 */
public class HomePageController implements Initializable {
    Stage stage;
    /**
     * Button to go to new page for army creation
     */
    @FXML
    private Button createArmyButton;

    /**
     * Output list for army one
     */
    @FXML
    private ListView armyOneList;

    /**
     * Output list for army two
     */
    @FXML
    private ListView armyTwoList;

    /**
     * Output label to display the winning army after simulation
     */
    @FXML
    private Label winningArmy;

    /**
     * Combobox to select which terrain battle will be taking place in
     */
    @FXML
    private ComboBox terrain;

    /**
     * filehandler used for loading armies
     */
    private WargamesFileHandler fileHandler = new WargamesFileHandler();

    /**
     * Army one is the left army in the GUI
     */
    private Army armyOne = new Army();

    /**
     * Army two is the right army in the GUI
     */
    private Army armyTwo = new Army();

    /**
     * The file selected by the user for the left army
     */
    private File armyOneFile;

    /**
     * the file selected by the user for the right army
     */
    private File armyTwoFile;

    /**
     * Boolean used for the toggle button for the left output list
     */
    private boolean toggleOne = false;

    /**
     * Boolean used for the toggle button for the left output list
     */
    private boolean toggleTwo = false;

    /**
     * When the create new army button is pressed this method loads the second FXML and sets the new scene
     * @param actionEvent
     * @throws IOException if the resource can't be loaded
     */
    public void handleCreateArmy(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("createNewArmy.fxml"));
        Stage window = (Stage) createArmyButton.getScene().getWindow();
        window.setScene(new Scene(root,600,400));
    }

    /**
     * When the left load button is pressed this method prompts user to select which file
     * from the Armies folder to be used. The contents of this file is then output to the left ListView
     */
    public void handleLoadArmyOne() {
        armyOneFile = getFileFromUser();
        try {
            armyOne = fileHandler.loadArmyFromFile(armyOneFile);
        }
        catch (IOException e){
            System.out.println("ERROR WHEN LOADING ARMY FILE");
        }
        writeArmyToListView(armyOne,armyOneList);
    }

    /**
     * When the right load button is pressed this method prompts user to select which file
     * from the Armies folder to be used. The contents of this file is then output to the right ListView
     */
    public void handleLoadArmyTwo(){
        armyTwoFile = getFileFromUser();
        try {
            armyTwo = fileHandler.loadArmyFromFile(armyTwoFile);
        }
        catch (IOException e){
            System.out.println("ERROR WHEN LOADING ARMY FILE");
        }
        writeArmyToListView(armyTwo,armyTwoList);

    }

    /**
     * method to prompt user with file selection
     * @return the selected file as File
     */
    public File getFileFromUser(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose an army");
        fileChooser.setInitialDirectory(new File("src/main/resources/Armies"));
        File file = fileChooser.showOpenDialog(stage);
        return file;
    }

    /**
     * Method to write army into listView
     * @param army the army to be written to listView
     * @param listView the ListView to be output to
     */
    public void writeArmyToListView(Army army, ListView listView){
        listView.getItems().clear();
        String armyName = army.getName();
        if(armyName.contains(" ")){
            armyName = armyName.replaceAll(" ","-");
        }
        listView.getItems().add("Army path: src/main/resources/Armies/" + armyName + ".csv");
        listView.getItems().add("Army name: " + army.getName());
        listView.getItems().add("Total units: " + army.getUnits().size());
        if(army.getInfantryUnits().size() > 0) {
            listView.getItems().add("Infantry - " + army.getInfantryUnits().get(0).getName() + ": " + army.getInfantryUnits().size());
        }
        if(army.getCavalryUnits().size() > 0) {
            listView.getItems().add("Cavalry - " + army.getCavalryUnits().get(0).getName() + ": " + army.getCavalryUnits().size());
        }
        if(army.getRangedUnits().size() > 0) {
            listView.getItems().add("Ranged - " + army.getRangedUnits().get(0).getName() + ": " + army.getRangedUnits().size());
        }
        if(army.getCommanderUnits().size() > 0) {
            listView.getItems().add("Commander - " + army.getCommanderUnits().get(0).getName() + ": " + army.getCommanderUnits().size());
        }
    }

    /**
     * Method to get terrain based on input from combobox
     * @return terrain as enum
     */
    public Terrain getTerrain(){
        if(terrain.getSelectionModel().getSelectedItem().toString().equals("Plains")){
            return Terrain.PLAINS;
        }
        if(terrain.getSelectionModel().getSelectedItem().toString().equals("Hill")){
            return Terrain.HILL;
        }
        if(terrain.getSelectionModel().getSelectedItem().toString().equals("Forest")){
            return Terrain.FOREST;
        }
        else{
            return null;
        }
    }

    /**
     * Method is run when the simulate button is pressed
     * Simulates a battle between the two selected armies
     * outputs winner to winningArmy label
     * updates the ListViews with postBattle army status
     */
    public void handleSimulate(){
        try {
            Army simArmyOne = armyOne;
            Army simArmyTwo = armyTwo;
            Battle simBattle = new Battle(simArmyOne, simArmyTwo);
            Army winner = simBattle.simulate(getTerrain());
            winningArmy.setText("Winner: " + winner.getName());
            if (winner.equals(simArmyOne)) {
                writeArmyToListView(simArmyOne, armyOneList);
                armyTwoList.getItems().clear();
                armyTwoList.getItems().add("Army name: " + simArmyTwo.getName());
                armyTwoList.getItems().add("Total units: " + simArmyTwo.getUnits().size());
            } else {
                writeArmyToListView(simArmyTwo, armyTwoList);
                armyOneList.getItems().clear();
                armyOneList.getItems().add("Army name: " + simArmyOne.getName());
                armyOneList.getItems().add("Total units: " + simArmyOne.getUnits().size());
            }
        }catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(e.getMessage());
            alert.setContentText("There needs to be two armies with units to battle");
            alert.showAndWait();
        }
    }

    /**
     * Method is run when the reset button is pressed
     * Reloads the armies from before the battle into ListView
     */
    public void handleReset(){
        try {
            armyOne = fileHandler.loadArmyFromFile(armyOneFile);
            armyTwo = fileHandler.loadArmyFromFile(armyTwoFile);
        }
        catch (IOException e){
            System.out.println("ERROR WHEN LOADING ARMY FILES");
        }
        writeArmyToListView(armyOne,armyOneList);
        writeArmyToListView(armyTwo,armyTwoList);
        winningArmy.setText("");
    }

    /**
     * Method is run when the left toggle button is pressed
     * if toggleOne is false then the left ListView is changed from displaying army info
     * to displaying the list of units in armyOne
     * if toggleOne is true then ListView displays the info of armyOne
     */
    public void handleToggleOne(){
        toggleListView(armyOne,armyOneList,toggleOne);
        if(!toggleOne){
            toggleOne = true;
        }else{
            toggleOne = false;
        }
    }

    /**
     * Method is run when the right toggle button is pressed
     * if toggleTwo is false then the right ListView is changed from displaying army info
     * to displaying the list of units in armyTwo
     * if toggleTwo is true then ListView displays the info of armyTwo
     */
    public void handleToggleTwo(){
        toggleListView(armyTwo,armyTwoList,toggleTwo);
        if(!toggleTwo){
            toggleTwo = true;
        }else{
            toggleTwo = false;
        }

    }

    /**
     * Outputs army list or army info depending on boolean toggle
     * @param army the selected army
     * @param listView the selected ListView
     * @param toggle if true then writes army info, if false the writes army units list
     */
    public void toggleListView(Army army, ListView listView, boolean toggle){
        if(!toggle){
            listView.getItems().clear();
            listView.getItems().addAll(army.getUnits());
        }else{
            writeArmyToListView(army,listView);
        }
    }

    /**
     * Initializes the combobox with hill as preselected value
     * Tried making the text color inside the combo box white to match with the rest of theme \
     * but was not able to
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        terrain.getItems().removeAll(terrain.getItems());
        terrain.getItems().addAll("Hill", "Plains", "Forest");
        terrain.setStyle("-fx-background-color: CHOCOLATE; -fx-text-fill: white");
        terrain.getSelectionModel().select("Hill");
    }
}
