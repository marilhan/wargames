package no.ntnu.idatg2001.wargames.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.idatg2001.wargames.Army;
import no.ntnu.idatg2001.wargames.UnitFactory;
import no.ntnu.idatg2001.wargames.Units.Unit;
import no.ntnu.idatg2001.wargames.WargamesFileHandler;

import java.io.IOException;
import java.util.ArrayList;

public class CreateArmyController {
    /**
     * Create new army button
     */
    @FXML
    private Button createArmyButton;

    /**
     * Cancel button
     */
    @FXML
    private Button cancelButton;

    /**
     * input text fields to be used for creating army
     */
    @FXML
    private TextField armyNameInput,
            infantryNameInput, infantryHealthInput, infantryAmountInput,
            cavalryNameInput, cavalryHealthInput, cavalryAmountInput,
            rangedNameInput, rangedHealthInput, rangedAmountInput,
            commanderNameInput, commanderHealthInput, commanderAmountInput;

    /**
     * the new army that gets created
     */
    Army newArmy = new Army();

    /**
     * Runs when the createArmyButton is pressed
     * Creates a new army based on the input in the TextFields and adds it to a new file
     * @throws IOException
     */
    public void handleCreateArmy() {
        WargamesFileHandler fileHandler = new WargamesFileHandler();
        try{
            newArmy.setName(armyNameInput.getText());
        }catch (IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(e.getMessage());
            alert.setContentText("Create a valid Army name");
            alert.showAndWait();
        }
        boolean hasUnits = false;
        if(infantryAmountInput.getText().contains(",") || infantryNameInput.getText().contains(",") || infantryHealthInput.getText().contains(",")
        || cavalryAmountInput.getText().contains(",") || cavalryNameInput.getText().contains(",") || cavalryHealthInput.getText().contains(",")
        || rangedAmountInput.getText().contains(",") || rangedNameInput.getText().contains(",") || rangedHealthInput.getText().contains(",")
        || commanderAmountInput.getText().contains(",") || commanderNameInput.getText().contains(",") || commanderHealthInput.getText().contains(",")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Illegal input");
            alert.setContentText("None of the inputs can have a comma");
            alert.showAndWait();
        }
        else {
            try {
                if (infantryAmountInput.getText() != "") {
                    hasUnits = true;
                    newArmy.addAll(makeUnitsList("InfantryUnit", Integer.parseInt(infantryAmountInput.getText()),
                            infantryNameInput.getText(), Integer.parseInt(infantryHealthInput.getText())));
                }

                if (cavalryAmountInput.getText() != "") {
                    hasUnits = true;
                    newArmy.addAll(makeUnitsList("CavalryUnit", Integer.parseInt(cavalryAmountInput.getText()),
                            cavalryNameInput.getText(), Integer.parseInt(cavalryHealthInput.getText())));
                }

                if (rangedAmountInput.getText() != "") {
                    hasUnits = true;
                    newArmy.addAll(makeUnitsList("RangedUnit", Integer.parseInt(rangedAmountInput.getText()),
                            rangedNameInput.getText(), Integer.parseInt(rangedHealthInput.getText())));
                }

                if (commanderAmountInput.getText() != "") {
                    hasUnits = true;
                    newArmy.addAll(makeUnitsList("CommanderUnit", Integer.parseInt(commanderAmountInput.getText()),
                            commanderNameInput.getText(), Integer.parseInt(commanderHealthInput.getText())));
                }
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(e.getMessage());
                alert.setContentText("Amount and health needs to be numbers");
                alert.showAndWait();
            }

            if (newArmy.hasUnits()) {
                try {
                    fileHandler.saveArmy(newArmy);
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Army created");
                    alert.setHeaderText("Army created");
                    alert.setContentText("Army has successfully been created");
                    alert.showAndWait();

                    Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("HomePage.fxml"));
                    Stage window = (Stage) createArmyButton.getScene().getWindow();
                    window.setScene(new Scene(root, 600, 400));

                } catch (IOException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(e.getMessage());
                    alert.setContentText("Change the name of the army to something unique");
                    alert.showAndWait();
                }
            }
            if (!hasUnits) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("No units of any type was added");
                alert.setContentText("Need to have a minimum of 1 unit for an army to be made");
                alert.showAndWait();
            }
        }
    }

    /**
     * Method to make a list of units using the input values
     * @param unitType the type of unit to be made
     * @param amount the amount to be made of said unit
     * @param name the name of the units
     * @param health the health of the units
     * @return an ArrayList of units
     */
    public ArrayList<Unit> makeUnitsList(String unitType, int amount, String name, int health){
        ArrayList<Unit> unitList = new ArrayList<>();
        if(name.isBlank()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Can't make units with no name");
            alert.setContentText("Add name to all units you want to make");
            alert.showAndWait();
        }
        if(health <= 0){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Health needs to be above 0");
            alert.setContentText("Make sure health is above 0 or the unit is already dead.");
            alert.showAndWait();
        }
        UnitFactory factory = new UnitFactory();
        try {
            unitList = factory.makeListOfUnits(unitType, amount, name, health);
        }
        catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        return unitList;
    }

    /**
     * If cancel button is pressed then go back to home page
     * @throws IOException if the resource can't load
     */
    public void handleCancel() throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("HomePage.fxml"));
        Stage window = (Stage) cancelButton.getScene().getWindow();
        window.setScene(new Scene(root,600,400));
    }
}
